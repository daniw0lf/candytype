package backend.cell;

import backend.Grid;
import backend.element.Food;
import backend.element.Nothing;
import backend.move.Direction;

/**
 * Created with IntelliJ IDEA.
 * User: daniel
 * Date: 01/06/13
 * Time: 14:10
 * To change this template use File | Settings | File Templates.
 */
public class CheckCell extends Cell {

    public CheckCell(Grid grid){
        super(grid);
    }
    public boolean fallUpperContent(){
        if (this.getAround()[Direction.UP.ordinal()].getContent().isCheckable()){    // TODO ta bien el instanceof ?
            this.getAround()[Direction.UP.ordinal()].getAndClearContent();
        }
        return super.fallUpperContent();
    }
}
