package backend;
import backend.GameState;

public class BasicCondition implements Conditions {
	private long requiredScore;
	private long maxMoves;
    private GameState gameState;
	
	public BasicCondition(long requiredScore, int maxMoves) {
		this.requiredScore = requiredScore;
		this.maxMoves = maxMoves;
	}

    public void setGameState(GameState gameState){
        this.gameState=gameState;
    }
	
	public boolean gameOver() {
		return gameState.getMoves() >= maxMoves;
	}
	
	public boolean playerWon() {
		return gameState.getScore() > requiredScore;
	}
}

