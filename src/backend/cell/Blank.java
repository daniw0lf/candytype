package backend.cell;

import backend.Grid;
import backend.element.Element;
import backend.move.Direction;

public class Blank extends Cell {

	public Blank(){
		
	}

	public boolean fallUpperContent() {
		Cell up = getAround()[Direction.UP.ordinal()];
        if(up instanceof Blank){
            return false;
        }

		Cell down = getAround()[Direction.DOWN.ordinal()];   //TODO Como se repite esto mismo en isEmpty puede que convenga una funcion auxiliar private
		while(down instanceof Blank){                       //TODO ver si se puede solucionar sin instanceof
            down = down.getAround()[Direction.DOWN.ordinal()];
        }

        if (!up.isEmpty() && up.isMovable()) {
            Element upperContent = up.getAndClearContent();
            down.setContent(upperContent);
            getGrid().wasUpdated();
            if (!down.isEmpty()) {
                getGrid().tryRemove(this);
                return true;
            } else {
                return down.fallUpperContent();
            }
        }

        return false;

 // hasFloor() SE FIJA SI ABAJO HAY UN ELEMENT, O SI ES NOTHING. SI ABAJO  ES NOTHING = FALSE, SI ES ELEMENT = TRUE.

/*        Cell up = around[Direction.UP.ordinal()];
        if (this.isEmpty() && !up.isEmpty() && up.isMovable()) {
            this.content = up.getAndClearContent();
            grid.wasUpdated();
            if (this.hasFloor()) {
                grid.tryRemove(this);
                return true;
            } else {
                Cell down = around[Direction.DOWN.ordinal()];
                return down.fallUpperContent();
            }
        }
        return false;
    }*/
	}
	public Blank(Grid grid) {
		super(grid);
	
	}
    
    public String getKey(){
        return "Blank";
    }
    
    public boolean isEmpty(){
        Cell down = getAround()[Direction.DOWN.ordinal()];
        while(down instanceof Blank){                       //TODO ver si se puede solucionar sin instanceof
            down = down.getAround()[Direction.DOWN.ordinal()];
        }
        return down.isEmpty();
    }
}
