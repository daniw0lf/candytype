package backend;

public class CompositeGameState implements Conditions{
	private Conditions gs1;
	private Conditions gs2;
	
	public CompositeGameState(Conditions gs1, Conditions gs2){
		this.gs1 = gs1;
		this.gs2 = gs2;
	}

	@Override
	public boolean gameOver() {
		return gs1.gameOver() || gs2.gameOver();
	}

	@Override
	public boolean playerWon() {
		return gs1.playerWon() && gs2.playerWon();
	}
	
	
}
