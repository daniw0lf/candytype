package backend;

import java.util.Map;

import backend.element.FoodType;

public class FoodCondition implements Conditions {
	private Map<FoodType, FoodTypeState> foodMap;
	
	public FoodCondition(Map<FoodType, FoodTypeState> foodMap){
		this.foodMap = foodMap;
	}

    public boolean gameOver() {
        return false;
    }

    public boolean playerWon() {
        for(FoodTypeState f: foodMap.values()){
        	if(!f.allReached()){
        		return false;
        	}
        }
        return true;
    }
}
