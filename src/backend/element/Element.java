package backend.element;


import backend.move.Direction;

public abstract class Element {
	
	public abstract boolean isMovable();
	
	public abstract String getKey();
	
	public String getFullKey() {
		return getKey();
	}

	public boolean isSolid() {
		return true;
	}
	
	public boolean isCheckable(){
		return false;
	}
	
	public abstract boolean isExplodable();

	public Direction[] explode() {
		return null;
	}
	
	public long getScore() {
		return 0;
	}
	
}
