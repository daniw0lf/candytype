package backend;

public class GameState implements Conditions {
	private long score = 0;
	private int moves = 0;
    private Conditions conditions;

    public GameState(Conditions conditions){
        this.conditions = conditions;
    }

	public void addScore(long value) {
		this.score = this.score + value;
	}

	public long getScore(){
		return score;
	}

	public void addMove() {
		moves++;
	}

	public int getMoves() {
		return moves;
	}

	public boolean gameOver(){
        return conditions.gameOver();
    }
	
	public boolean playerWon(){
        return conditions.playerWon();
    }

}
