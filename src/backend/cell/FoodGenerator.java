package backend.cell;

import java.util.HashMap;
import java.util.Map;

import backend.FoodTypeState;
import backend.Grid;
import backend.element.Element;
import backend.element.Food;
import backend.element.FoodType;

public class FoodGenerator  extends CandyGeneratorCell{
	private Map<FoodType, FoodTypeState> foodMap;
	public FoodGenerator(Grid grid, Map<FoodType, FoodTypeState> foodMap) {
		super(grid);
		this.foodMap = foodMap;
		
	}

	public Element getContent(){
		if((int)(Math.random()*100) % 10== 0){
			int j = (int)(Math.random() * FoodType.values().length);
			FoodTypeState qty=foodMap.get(FoodType.values()[j]);
			if(qty.foodRemain()>0){
				qty.draw();
				return new Food(FoodType.values()[j]);
			}
		}
		return super.getContent();
	}
}	

