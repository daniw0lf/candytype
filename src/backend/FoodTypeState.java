package backend;

public class FoodTypeState {
	private int remain;
	private int reachedCheckCell;
	private final int startingAmount;
	
	
	public FoodTypeState(int quantity)
	{
		this.remain=quantity;
		this.reachedCheckCell=0;
		this.startingAmount = quantity;
	}
	
	public void hasReachedFloor(){
	this.reachedCheckCell+=1;
	}
	
	public boolean allReached(){
		return startingAmount - reachedCheckCell == 0;
	}
	
	public int foodRemain(){
		return this.remain;
	}
	public void draw(){
		this.remain-=1;
	}

}
